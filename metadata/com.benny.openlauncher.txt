Categories:System
License:Apache-2.0
Web Site:https://github.com/BennyKok/OpenLauncher/blob/HEAD/README.md
Source Code:https://github.com/BennyKok/OpenLauncher
Issue Tracker:https://github.com/BennyKok/OpenLauncher/issues

Auto Name:OpenLauncher
Summary:Launch applications and manage homescreen
Description:
Launcher that aims to be a powerful and community driven project.
.

Repo Type:git
Repo:https://github.com/BennyKok/OpenLauncher

Build:alpha1-patch1,2
    commit=32d793ccac3380f022feb9019ee63dc381ff6bf2
    subdir=launcher
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:alpha1-patch1
Current Version Code:2
